## Installation

* Clone the source code to your machine
* Create a new database called `DemoDb` and execute the seed script `DatabaseInit.sql`
* Ensure your Windows login account has permissions to read/write/create SQL objects to `DemoDb`
* Open a shell and navigate to the source code directory
* Execute `dotnet restore` followed by `dotnet build` and finally `dotnet run --project Demo`

Project should then be visible at the URL output by the `dotnet run` command

## API

You can get a list of room/status combinations by running the following curl command

`curl https://localhost:44307/api/jobs`
