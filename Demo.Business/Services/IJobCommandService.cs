﻿using Demo.Business.Models;
using Demo.Business.Services.ServiceModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Demo.Business.Services
{
    public interface IJobCommandService
    {
        Task<bool> MarkJobAsCompleteAsync(Guid jobId);
    }

    public class JobCommandService : IJobCommandService
    {
        private readonly DemoDbContext dbContext;

        public JobCommandService(DemoDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<bool> MarkJobAsCompleteAsync(Guid jobId)
        {
            var job = await dbContext.RxJobs.SingleOrDefaultAsync(j => j.Id == jobId);

            if (job == null)
            {
                return false;
            }

            job.StatusNum = (int)JobStatus.Complete;

            return (await dbContext.SaveChangesAsync()) > 0;
        }
    }
}
