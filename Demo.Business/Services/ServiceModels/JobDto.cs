﻿using System;

namespace Demo.Business.Services.ServiceModels
{
    public class JobDto
    {
        public Guid Id { get; internal set; }
        public string Name { get; internal set; }
        public int? Floor { get; internal set; }
        public string Status { get; internal set; }
        public string RoomTypeName { get; internal set; }
        public bool CanBeCompleted { get; internal set; }
        public JobStatus? JobStatus { get; internal set; }
    }
}