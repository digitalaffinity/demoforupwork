﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.Business.Services.ServiceModels
{
    public class JobStatusSummaryDto
    {
        public string RoomName { get; internal set; }
        public string Status { get; internal set; }
        public int Count { get; internal set; }
    }
}
