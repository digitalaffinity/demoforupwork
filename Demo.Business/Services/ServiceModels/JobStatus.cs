﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.Business.Services.ServiceModels
{
    public enum JobStatus
    {
        NotStarted = 2,
        InProgress = 3,
        Delayed = 4,
        Complete = 1,
        Unknown = 0
    }
}
