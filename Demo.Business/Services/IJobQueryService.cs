﻿using Demo.Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Demo.Business.Services.ServiceModels;
using Microsoft.EntityFrameworkCore;
using System;

namespace Demo.Business.Services
{
    public interface IJobQueryService
    {
        Task<IEnumerable<JobDto>> GetAllJobsAsync();

        Task<IEnumerable<JobStatusSummaryDto>> GetJobStatusSummaryAsync();
    }

    public class JobQueryService : IJobQueryService
    {
        private readonly DemoDbContext dbContext;

        public JobQueryService(DemoDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<IEnumerable<JobDto>> GetAllJobsAsync()
        {
            var query =
                from job in dbContext.RxJobs
                join roomType in dbContext.RxRoomTypes on job.RoomTypeId equals roomType.Id
                select new JobDto
                {
                    Id = job.Id,
                    Name = job.Name,
                    Floor = job.Floor,
                    Status = job.Status,
                    JobStatus = (JobStatus)job.StatusNum,
                    CanBeCompleted = job.CanBeCompleted,
                    RoomTypeName = roomType.Name
                };

            return await query.ToListAsync();
        }

        public async Task<IEnumerable<JobStatusSummaryDto>> GetJobStatusSummaryAsync()
        {
            var query =
               from job in dbContext.RxJobs
               join roomType in dbContext.RxRoomTypes on job.RoomTypeId equals roomType.Id
               select new
               {
                    RoomName = roomType.Name,
                    job.Status
               };

            var summaryQuery = query
                .GroupBy(j => new { j.RoomName, j.Status });

            return summaryQuery.Select(j => new JobStatusSummaryDto
            {
                RoomName = j.Key.RoomName,
                Status = j.Key.Status,
                Count = j.Count()
            });
        }
    }
}
