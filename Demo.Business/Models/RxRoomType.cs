﻿using System;

#nullable disable

namespace Demo.Business.Models
{
    public partial class RxRoomType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
