﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace Demo.Business.Models
{
    public partial class RxJob
    {
        public Guid Id { get; set; }
        public int? ContractorId { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public int? Floor { get; set; }
        public int? Room { get; set; }
        public string DelayReason { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? DateCompleted { get; set; }
        public DateTime? DateDelayed { get; set; }
        public int? StatusNum { get; set; }

        [NotMapped]
        public bool CanBeCompleted => StatusNum == (int)Services.ServiceModels.JobStatus.Delayed || StatusNum == (int)Services.ServiceModels.JobStatus.InProgress;

        public int? RjobId { get; set; }
        public Guid? RoomTypeId { get; set; }
    }
}
