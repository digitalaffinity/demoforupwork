﻿using Demo.Business.Services.ServiceModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Demo.Business
{
    public static class StatusToColourUtil
    {
        public static string GetColourForStatus(JobStatus? status)
        {
            switch (status)
            {
                case JobStatus.NotStarted:
                    return "blue";
                
                case JobStatus.InProgress:
                    return "purple";
                    
                case JobStatus.Delayed:
                    return "orange";

                case JobStatus.Complete:
                    return "green";

                default:
                    return "";
            }
        }
    }
}
