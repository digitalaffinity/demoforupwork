﻿using Demo.Business.Services;
using Demo.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IJobQueryService jobQueryService;
        private readonly IJobCommandService jobCommandService;

        public HomeController(
            ILogger<HomeController> logger, 
            IJobQueryService jobQueryService,
            IJobCommandService jobCommandService)
        {
            _logger = logger;
            this.jobQueryService = jobQueryService;
            this.jobCommandService = jobCommandService;
        }

        [HttpPost]
        public async Task<IActionResult> Index(Guid id)
        {
            var resultSuccess = await jobCommandService.MarkJobAsCompleteAsync(id);

            if (!resultSuccess)
            {
                ModelState.AddModelError("DbUpate", "An error occurred while trying to update");
            }

            return await Index();
        }

        public async Task<IActionResult> Index()
        {
            var jobs = await jobQueryService.GetAllJobsAsync();

            var viewModels = jobs.Select(j => new JobStatusViewModel(j));

            return View(viewModels);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
