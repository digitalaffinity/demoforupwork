﻿using Demo.Business.Services;
using Demo.Business.Services.ServiceModels;
using Demo.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Demo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobsController : ControllerBase
    {
        private readonly IJobQueryService jobQueryService;

        public JobsController(IJobQueryService jobQueryService)
        {
            this.jobQueryService = jobQueryService;
        }

        [HttpGet]
        public async Task<IEnumerable<JobStatusSummaryDto>> Get()
        {
            return await jobQueryService.GetJobStatusSummaryAsync();
        }
    }
}
