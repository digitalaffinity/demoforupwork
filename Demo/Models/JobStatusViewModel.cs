﻿using Demo.Business;
using Demo.Business.Services.ServiceModels;
using System;

namespace Demo.Models
{
    public class JobStatusViewModel
    {
        public JobStatusViewModel(JobDto j)
        {
            Id = j.Id;
            Floor = j.Floor;
            Name = j.Name;
            RoomTypeName = j.RoomTypeName;
            StatusName = j.Status;
            JobStatus = j.JobStatus;
            CanBeCompleted = j.CanBeCompleted;
        }

        public Guid Id { get; }
        public string Name { get; }
        public int? Floor { get; }
        public JobStatus? JobStatus{ get; }
        public string StatusName { get; }
        public bool CanBeCompleted { get; }

        public string StatusColour
        {
            get
            {
                return StatusToColourUtil.GetColourForStatus(JobStatus);
            }
        }
        public string RoomTypeName { get; }
    }
}
